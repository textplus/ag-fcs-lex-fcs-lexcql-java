# LexCQL Java Validator (and Parser)

This library is a wrapper around the [cql-java](https://github.com/indexdata/cql-java) library for CQL _(Contextual Query Language)_ parsing and add validation for LexCQL (CQL for Lexical Resources, specified in Text+).

## Dependencies

* `cql-java` &rarr; `org.z3950.zing:cql-java:1.12`. See [source repo](https://github.com/indexdata/cql-java) for more information.
* `junit 4` for tests.

## Building

Build the library with:
```bash
mvn clean package
```
The built jar will be [`target/lexcql-1.0-SNAPSHOT.jar`](target/lexcql-1.0-SNAPSHOT.jar).

## Usage

Add the `lexcql` dependency to your `pom.xml`:

```xml
<!-- add to project > dependencies -->
<dependency>
    <groupId>org.textplus.fcs</groupId>
    <artifactId>lexcql</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```

Then, in your code, you can validate parsed queries using `LexCQLValidator.validate(..)`.
```java
// imports
import org.textplus.fcs.LexCQLValidationException;
import org.textplus.fcs.LexCQLValidator;
import org.z3950.zing.cql.CQLNode;
import org.z3950.zing.cql.CQLParser;

// ...

// parse query
CQLParser parser = new CQLParser();
String query = "...";
CQLNode node = parser.parse(query)

// validate parsed query
try {
    LexCQLValidator.validate(node);
} catch (LexCQLValidationException e) {
    // ...
}
```
