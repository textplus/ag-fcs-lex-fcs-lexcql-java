package org.textplus.fcs;

import static org.junit.Assert.assertThrows;

import java.io.IOException;

import org.junit.Test;
import org.junit.function.ThrowingRunnable;
import org.z3950.zing.cql.CQLParseException;
import org.z3950.zing.cql.CQLParser;

public class LexCQLValidatorTest {
    @Test
    public void testValidateSimple() throws CQLParseException, IOException, LexCQLValidationException {
        final CQLParser parser = new CQLParser();

        LexCQLValidator.validate(parser.parse("Maus"));
        LexCQLValidator.validate(parser.parse("Katze"));
    }

    @Test
    public void testValidateBoolean() throws CQLParseException, IOException, LexCQLValidationException {
        final CQLParser parser = new CQLParser();

        LexCQLValidator.validate(parser.parse("Katze OR Hund"));
        LexCQLValidator.validate(parser.parse("(Katze AND Hund)"));
        LexCQLValidator.validate(parser.parse("Katze OR (Hund)"));
        LexCQLValidator.validate(parser.parse("(Katze OR (Hund)) AND Fisch"));

        assertThrows(LexCQLValidationException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                LexCQLValidator.validate(parser.parse("Katze NOT Hund"));
            }
        });
        assertThrows(LexCQLValidationException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                LexCQLValidator.validate(parser.parse("Katze PROX Hund"));
            }
        });
    }

    @Test
    public void testValidateBooleanModifier() throws CQLParseException, IOException, LexCQLValidationException {
        final CQLParser parser = new CQLParser();

        LexCQLValidator.validate(parser.parse("Katze OR Hund"));
        LexCQLValidator.validate(parser.parse("(Katze AND Hund)"));

        assertThrows(LexCQLValidationException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                LexCQLValidator.validate(parser.parse("Katze AND/some.mod Hund"));
            }
        });
        assertThrows(LexCQLValidationException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                LexCQLValidator.validate(parser.parse("Katze or/rel.combine=sum Hund"));
            }
        });
    }

    @Test
    public void testValidatePOSUD17Values() throws CQLParseException, IOException, LexCQLValidationException {
        final CQLParser parser = new CQLParser();

        LexCQLValidator.validate(parser.parse("pos = NOUN"));
        LexCQLValidator.validate(parser.parse("pos = nOuN"));
        LexCQLValidator.validate(parser.parse("(pos = VERB)"));
        LexCQLValidator.validate(parser.parse("pos = \"ADJ\""));
        LexCQLValidator.validate(parser.parse("(Katze OR (pos = AUX)) AND Fisch"));

        for (String pos : LexCQLValidator.POS_UD17) {
            String query = "pos = " + pos;
            LexCQLValidator.validate(parser.parse(query));
        }

        assertThrows(LexCQLValidationException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                LexCQLValidator.validate(parser.parse("pos = apple"));
            }
        });
    }

    @Test
    public void testValidateTermRelation() throws CQLParseException, IOException, LexCQLValidationException {
        final CQLParser parser = new CQLParser();

        LexCQLValidator.validate(parser.parse("pos = NOUN"));
        LexCQLValidator.validate(parser.parse("lemma = abc"));
        LexCQLValidator.validate(parser.parse("def = \"A long definition ...\""));

        final String[] fields = { "pos", "lemma", "def" };
        // NOTE: "exact" and "scr" will fail -> will be parsed into sth. like
        // `cql.serverChoice = "pos exact value"` (or "... src ...")
        final String[] relations = { "<", ">", "<=", ">=", "<>", "==", "cql.any", "any", "all" };
        for (final String field : fields) {
            for (final String relation : relations) {
                assertThrows(LexCQLValidationException.class, new ThrowingRunnable() {
                    @Override
                    public void run() throws Throwable {
                        String query = field + " " + relation + " value";
                        LexCQLValidator.validate(parser.parse(query));
                    }
                });
            }
        }
    }

    @Test
    public void testValidateTermRelationModifiers() throws CQLParseException, IOException, LexCQLValidationException {
        final CQLParser parser = new CQLParser();

        LexCQLValidator.validate(parser.parse("pos = NOUN"));
        LexCQLValidator.validate(parser.parse("lemma = apple"));

        assertThrows(LexCQLValidationException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                LexCQLValidator.validate(parser.parse("pos =/bad apple"));
            }
        });
        assertThrows(LexCQLValidationException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                LexCQLValidator.validate(parser.parse("pos =/xyz.unit=\"street\" apple"));
            }
        });
    }

    @Test
    public void testValidatePrefixAssignment() throws CQLParseException, IOException, LexCQLValidationException {
        final CQLParser parser = new CQLParser();

        assertThrows(LexCQLValidationException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                LexCQLValidator.validate(parser.parse("> dc = \"info:srw/context-sets/1/dc-v1.1\" dc.title any fish"));
            }
        });
    }

    @Test
    public void testValidateSorting() throws CQLParseException, IOException, LexCQLValidationException {
        final CQLParser parser = new CQLParser();

        assertThrows(LexCQLValidationException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                LexCQLValidator
                        .validate(parser.parse("\"dinosaur\" sortBy dc.date/sort.descending dc.title/sort.ascending"));
            }
        });
    }
}
