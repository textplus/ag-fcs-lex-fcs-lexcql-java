package org.textplus.fcs;

import static org.junit.Assert.assertThrows;

import java.io.IOException;

import org.junit.Test;
import org.junit.function.ThrowingRunnable;
import org.z3950.zing.cql.CQLParseException;

public class LexCQLParserTest {
    @Test
    public void testParse() throws CQLParseException, IOException {
        final LexCQLParser parser = new LexCQLParser();

        parser.parse("Katze OR Hund");
        parser.parse("(Katze AND Hund)");

        assertThrows(IOException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                parser.parse("Katze NOT Hund");
            }
        });

        assertThrows(IOException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                parser.parse("pos = unknownpos");
            }
        });
    }
}
