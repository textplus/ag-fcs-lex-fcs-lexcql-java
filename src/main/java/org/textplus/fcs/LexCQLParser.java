package org.textplus.fcs;

import java.io.IOException;

import org.z3950.zing.cql.CQLNode;
import org.z3950.zing.cql.CQLParseException;
import org.z3950.zing.cql.CQLParser;

public class LexCQLParser extends CQLParser {
    @Override
    public CQLNode parse(String cql) throws CQLParseException, IOException {
        CQLNode node = super.parse(cql);
        try {
            LexCQLValidator.validate(node);
        } catch (LexCQLValidationException e) {
            // TODO: better exception handling
            throw new IOException("LexCQL validation error", e);
        }
        return node;
    }

    // TODO: hijack main of CQLParser?
}
