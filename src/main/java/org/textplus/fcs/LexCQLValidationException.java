package org.textplus.fcs;

public class LexCQLValidationException extends Exception {

    public LexCQLValidationException(String message) {
        super(message);
    }

}
