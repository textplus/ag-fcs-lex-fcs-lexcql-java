package org.textplus.fcs;

import java.util.Arrays;

import org.z3950.zing.cql.CQLAndNode;
import org.z3950.zing.cql.CQLBooleanNode;
import org.z3950.zing.cql.CQLNode;
import org.z3950.zing.cql.CQLNotNode;
import org.z3950.zing.cql.CQLOrNode;
import org.z3950.zing.cql.CQLPrefixNode;
import org.z3950.zing.cql.CQLProxNode;
import org.z3950.zing.cql.CQLRelation;
import org.z3950.zing.cql.CQLSortNode;
import org.z3950.zing.cql.CQLTermNode;

public class LexCQLValidator {
    protected final static String[] INDEXES = { "lemma", "pos", "def" };
    protected final static String[] POS_UD17 = { "ADJ", "ADP", "ADV", "AUX", "CCONJ", "DET", "INTJ", "NOUN", "NUM",
            "PART", "PRON", "PROPN", "PUNCT", "SCONJ", "SYM", "VERB", "X" };
    static {
        Arrays.sort(INDEXES);
        Arrays.sort(POS_UD17);
    }

    public static void validate(CQLNode node) throws LexCQLValidationException {
        if (node == null) {
            return;
        }

        if (node instanceof CQLTermNode) {
            CQLTermNode tnode = (CQLTermNode) node;
            // check index / field
            if (tnode.getIndex() != null &&
                    !tnode.getIndex().equalsIgnoreCase("srw.serverChoice") &&
                    !tnode.getIndex().equalsIgnoreCase("cql.serverChoice")) {
                // check index (field)
                if (Arrays.binarySearch(INDEXES, tnode.getIndex().toLowerCase()) < 0) {
                    throw new LexCQLValidationException("Unknown index: " + tnode.getIndex());
                }

                // check relation
                validateRelation(tnode);

                // check term (value) (e.g. if restricted set of words, like POS tags)
                if (tnode.getIndex().equalsIgnoreCase("pos")) {
                    // check that term (value) is valid UD17 pos tag
                    if (Arrays.binarySearch(POS_UD17, tnode.getTerm().toUpperCase()) < 0) {
                        throw new LexCQLValidationException(
                                "Unknown UD17 POS value for index 'pos': " + tnode.getTerm());
                    }
                }
            }
        } else if (node instanceof CQLBooleanNode) {
            CQLBooleanNode bnode = (CQLBooleanNode) node;
            // check operation - AND, OR
            if (bnode instanceof CQLNotNode) {
                throw new LexCQLValidationException("Unsupported boolean operator: NOT");
            } else if (bnode instanceof CQLProxNode) {
                throw new LexCQLValidationException("Unsupported boolean operator: PROX");
            } else if (!(bnode instanceof CQLAndNode || bnode instanceof CQLOrNode)) {
                throw new LexCQLValidationException(
                        "Unsupported unknown boolean operator: " + bnode.getClass().getSimpleName());
            }
            // check modifiers
            validateModifiers(bnode);
            validate(bnode.getLeftOperand());
            validate(bnode.getRightOperand());
        } else if (node instanceof CQLPrefixNode) {
            throw new LexCQLValidationException("Prefix syntax is not supported!");
        } else if (node instanceof CQLSortNode) {
            throw new LexCQLValidationException("Sort syntax is not supported!");
        }
    }

    private static void validateRelation(CQLTermNode node) throws LexCQLValidationException {
        CQLRelation rnode = node.getRelation();

        // check relation
        if (!rnode.getBase().equalsIgnoreCase("=")) {
            throw new LexCQLValidationException("Term relation not allowed: " + rnode.getBase());
        }

        validateModifiers(node);
    }

    private static void validateModifiers(CQLTermNode node) throws LexCQLValidationException {
        // validate modifiers of term relation
        if (!node.getRelation().getModifiers().isEmpty()) {
            throw new LexCQLValidationException("Term relation does not allow modifiers!");
        }
    }

    private static void validateModifiers(CQLBooleanNode node) throws LexCQLValidationException {
        // validate modifiers of boolean operator
        if (node.getModifiers().size() > 0) {
            throw new LexCQLValidationException("Boolean relation does not allow modifiers!");
        }
    }
}
